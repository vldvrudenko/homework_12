"use strict";

const btnStop = document.getElementsByClassName('stop')[0];
const btnContinue = document.getElementsByClassName('continue')[0];
const images = document.getElementsByClassName('image-to-show');

let flag = true, 
    counter = 0, 
    counterStop;

images[counter].style.display = "block";

function continueShow() {
    const timer = setInterval(continueShowInner, 3000);

    function continueShowInner() {
        if (!flag) {
            counter = counterStop;
            flag = true;
        };
        images[counter].style.display = "none";
        counter = (counter + 1) % images.length;
        images[counter].style.display = "block";
    };

    btnStop.addEventListener("click", stopShow);
    function stopShow() {
        clearTimeout(timer);
        counterStop = counter;
        flag = false;
    };
};

btnContinue.addEventListener("click", continueShow);

continueShow();